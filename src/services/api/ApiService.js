import axios from 'axios';
const API_URL = 'http://192.168.99.100:8081';

export class APIService{

constructor(){
}


getProducts() {
    const url = `${API_URL}/products`;
    return axios.get(url).then(response => response.data);
   }
}