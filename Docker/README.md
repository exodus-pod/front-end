# web-shop

## Containerizng the front-end 

On your local machine First Build the artifcats from the project

```
npm run build
```

## example Compiled Project Files                                      
```
  dist/js/chunk-vendors.65ddc463.js  
  dist/js/app.502e184f.js                  
  dist/js/chunk-1ee44b1b.3b785123.js       
  dist/service-worker.js                   
  dist/precache-manifest.ec1ccb6b450502d   
  1bf817a3a199e07b7.js
  dist/js/about.ad763791.js                
  dist/css/chunk-vendors.3ae4e051.css      
  dist/css/app.9e2583d2.css                
  dist/css/chunk-1ee44b1b.2e6a182e.css    
```

The artifacts will be copied to the /usr/src/app to be serverd from http server

The container will install http-server and application will be strated
